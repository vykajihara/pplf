:- [cadastro].
:- [diagnostico].

init :-
    nl, nl, nl, write('
    Bem-vindo(a)!\n
    Escolha uma opção:
        1 - Cadastros.
        2 - Diagnósticos.
        3 - Sair.'), nl, nl, nl,
    read_string(user_input, "\n", "\r\t", _, Op),
    executar_opcao(Op).

executar_opcao(Op) :-
    Op == "1" , cadastro_menu, init;
    Op == "2" , diagnostico_menu, init;
    Op == "3" , nl, nl, nl, write('Até logo!'), nl, nl, nl, halt.


