:- use_module(library(plunit)).

cadastro_menu :-
    cadastro_init,
    nl, nl, nl, write('
    Selecione uma opção:
        1 - Novo cadastro.
        2 - Consultar cadastro.
        3 - Alterar cadastro.
        4 - Excluir cadastro.
        5 - Retornar ao menu principal.'), nl, nl, nl,
    read_string(user_input, "\n", "\r\t", _, Op),
    executar_opcao_cadastro(Op).

executar_opcao_cadastro(Op) :-
    Op == "1", novo_cadastro,      gravar_registros, cadastro_menu;
    Op == "2", consultar_cadastro, cadastro_menu;
    Op == "3", alterar_cadastro,   gravar_registros, cadastro_menu;
    Op == "4", excluir_cadastro,   gravar_registros, cadastro_menu;
    Op == "5", init.

escrever_registro([N, E, T, D, TS]) :-
    nl, nl, nl, write('    Dados:'), nl, nl,
    write('    Nome: '),               write(N), nl,
    write('    Endereço: '),           write(E), nl,
    write('    Telefone: '),           write(T), nl,
    write('    Data de nascimento: '), write(D), nl,
    write('    Tipo sanguíneo: '),     write(TS), nl, nl, nl,
    write('Pressione ENTER para continuar...'),
    read_string(user_input, "\n", "\r\t", _, _).

%% cadastro_init
% carrega os dados gravados no .txt dos paciente que ja tem cadastro
%
cadastro_init :-
    retractall(paciente(_, _)),
    exists_file('pacientes.txt'),
    open('pacientes.txt', read, P),
    ler_arquivo(P, L),
    close(P),
    assert_lista(L).

ler_arquivo(Stream, []) :-
    at_end_of_stream(Stream), !.
ler_arquivo(Stream, [X | L]) :- !,
    \+ at_end_of_stream(Stream),
    read_line_to_codes(Stream, Codes),
    atom_codes(Atom, Codes),
    atom_string(Atom, X),
    ler_arquivo(Stream, L), !.

assert_lista([]).
assert_lista([X | L]) :-
    term_string(Term, X),
    assert(Term),
    assert_lista(L).

novo_cadastro :-
    get_cpf(CPF),
    paciente(CPF, Info)
    -> ( nl, nl, nl, write('ERRO: CPF com cadastro existente!'), nl,
         escrever_registro(Info))
    ; ( get_cpf(CPF),
        get_info(Nome, Endereco, Telefone, DataNasc, TipoSanguineo)),
    % Aqui, acho que porque o fluxo de execucao foi alterado, o
    % primeiro CPF que foi requisitado. Entao para gravar o novo
    % cadastro, foi necessario pedir o valor novamente.
    assert(paciente(CPF, [Nome, Endereco, Telefone, DataNasc, TipoSanguineo])).

/* FIX ME!!! Perguntar o campo a ser alterado */
alterar_cadastro :-
    get_cpf(CPF),
    get_info(Nome, Endereco, Telefone, DataNasc, TipoSang),
    retract(paciente(CPF, _)),
    assert(paciente(CPF, [Nome, Endereco, Telefone, DataNasc, TipoSang])).

/* FIX ME!!! Incluir confirmação de remoção */
excluir_cadastro :-
    get_cpf(CPF),
    retract(paciente(CPF, _)),
    nl, nl, nl, write('Cadastro excluído com sucesso.').

consultar_cadastro :-
    get_cpf(CPF),
    paciente(CPF, Info)
    -> escrever_registro(Info)
    ; write('Cadastro não encontrado!').

get_cpf(CPF) :-
    nl, nl, nl, write('Por favor, informe seu CPF (somente números): '),
    read_string(user_input, "\n", "\r\t", _, CPF).

get_info(Nome, Endereco, Telefone, DataNasc, TipoSanguineo) :-
    write('Nome completo: '),
    read_string(user_input, "\n", "\r\t", _, Nome),
    write('Endereco: '),
    read_string(user_input, "\n", "\r\t", _, Endereco),
    write('Telefone: '),
    read_string(user_input, "\n", "\r\t", _, Telefone),
    write('Data de nascimento: '),
    read_string(user_input, "\n", "\r\t", _, DataNasc),
    write('Tipo sanguíneo: '),
    read_string(user_input, "\n", "\r\t", _,TipoSanguineo).

/* FIX-ME!!! Quando nao ha pacientes na memoria, setof retorna false.
             Neste caso, o programa abre o arquivo em modo write e fecha
             (ou seja, apaga todo o conteudo) */
gravar_registros :-
    \+ setof([C, I], paciente(C, I), _),
    open('pacientes.txt', write, P),
    close(P).
gravar_registros :-
    setof([C, I], paciente(C, I), Lista),
    formatar_registros(Lista, LFormat),
    open('pacientes.txt', write, P),
    current_output(Stream),
    set_output(P),
    escrever_registros(LFormat),
    close(P),
    set_output(Stream).

escrever_registros([]).
escrever_registros([X | L]) :-
    write(X), nl,
    escrever_registros(L).

formatar_registros([], []).
formatar_registros([[CPF, Info] | L1], [Y | L2]) :-
    info_list_to_atom(Info, A),
    atom_concat('paciente("', CPF, S1),
    atom_concat(S1, '", ', S2),
    atom_concat(S2, A, S3),
    atom_concat(S3, ')', Y),
    formatar_registros(L1, L2).

info_list_to_atom([N, E, T, D, TS], A) :-
    atom_concat('["', N, S1),
    atom_concat(S1, '", "', S2),
    atom_concat(S2, E, S3),
    atom_concat(S3, '", "', S4),
    atom_concat(S4, T, S5),
    atom_concat(S5, '", "', S6),
    atom_concat(S6, D, S7),
    atom_concat(S7, '", "', S8),
    atom_concat(S8, TS, S9),
    atom_concat(S9, '"]', A).


:- begin_tests(modulo_cadastro).

test(info_list_to_atom_1) :- info_list_to_atom(["Nome", "Endereco", "Telefone", "DataDeNascimento", "TipoSanguineo"],
                                               '["Nome", "Endereco", "Telefone", "DataDeNascimento", "TipoSanguineo"]').

test(formatar_registros_1) :- formatar_registros([], []).
test(formatar_registros_2) :- formatar_registros([[123, ["A", "B", "C", "D", "E"]],
                                                  [456, ["1", "2", "3", "4", "5"]]],
                                                 ['paciente(123, ["A", "B", "C", "D", "E"])',
                                                  'paciente(456, ["1", "2", "3", "4", "5"])']).

:- end_tests(modulo_cadastro).






