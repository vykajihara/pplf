:- use_module(library(plunit)).

% Dados

% Doenças neurológicas
%
doenca('Cefaleia',  ['Dor de cabeça',
                     'Sensibilidade à luz',
                     'Sensibilidade aos sons',
                     'Sensibilidade aos cheiros',
                     'Irritabilidade',
                     'Náuseas e vômitos',
                     'Queda da pálpebra',
                     'Sensação de latejamento',
                     'Dor que fica mais intensa ao movimentar o corpo']).

doenca('Acidente vascular encefálico', ['Perda súbita de força',
                                        'Formigamento de um lado do corpo, atingindo rosto, braço ou perna',
                                        'Paralisia facial',
                                        'Dificuldade de falar',
                                        'Dificuldade para caminhar',
                                        'Dor de cabeça',
                                        'Perda de visão repentina em um dos olhos',
                                        'Vertigem',
                                        'Convulsões',
                                        'Desmaios',
                                        'Desequilíbrio',
                                        'Náuseas e vômitos']).

doenca('Dorsalgia', ['Dor nas costas',
                     'Pontadas nas costas',
                     'Sensação de queimação na coluna',
                     'Dificuldade para respirar']).

% Doencas cardiológicas
%
doenca('Dor precordial', ['Dor aguda no peito']).

doenca('Insuficiência cardíaca', ['Palpitações',
                                  'Falta de ar',
                                  'Cansaço (mesmo em repouso)',
                                  'Tosse',
                                  'Inchaço em diferentes áreas do corpo (extremidades e abdômem)',
                                  'Aumento de peso',
                                  'Falta de apetite',
                                  'Pulso rápido',
                                  'Problemas para dormir',
                                  'Anemia',
                                  'Confusão mental',
                                  'Aumento da vontade de urinar durante a noite']).

doenca('Hipertensão arterial', ['Pressão alta (14 por 9 ou mais)',
                                'Dor de cabeça',
                                'Falta de ar',
                                'Visão borrada',
                                'Zumbido no ouvido',
                                'Tontura',
                                'Dor no peito']).

doenca('Arritmia', ['Palpitações',
                    'Pulso rápido',
                    'Desmaios',
                    'Tontura',
                    'Falta de ar', 'Cansaço (mesmo em repouso)',
                    'Sensação de aperto no peito',
                    'Sensação de que o coração para de bater momentaneamente']).

% Doenças pneumológicas
doenca('Asma', ['Tosse',
                'Produção de muco',
                'Sensação de sufoco',
                'Chiado no peito',
                'Desconforto torácico',
                'Respiração rápida e curta']).

doenca('Bronquite', ['Tosse',
                     'Produção de muco',
                     'Chiado no peito',
                     'Sensação de peito carregado',
                     'Febre',
                     'Cansaço (mesmo em repouso)',
                     'Falta de ar',
                     'Dificuldade de falar',
                     'Irritação na garganta',
                     'Coriza',
                     'Lábios roxos']).

doenca('Rinite', ['Coriza',
                  'Nariz entupido',
                  'Espirros constantes',
                  'Coceira nos olhos',
                  'Coceira no nariz']).

% Sintomas
%
sintoma('Anemia').
sintoma('Aumento de peso').
sintoma('Aumento da vontade de urinar durante a noite').
sintoma('Convulsões').
sintoma('Coração acelerado').
sintoma('Confusão mental').
sintoma('Chiado no peito').
sintoma('Coriza').
sintoma('Coceira no nariz').
sintoma('Coceira nos olhos').
sintoma('Palpitações').
sintoma('Paralisia facial').
sintoma('Perda de visão repentina em um dos olhos').
sintoma('Perda súbita de força').
sintoma('Pontadas nas costas').
sintoma('Problemas para dormir').
sintoma('Pressão alta (14 por 9 ou mais)').
sintoma('Pulso rápido').
sintoma('Produção de muco').
sintoma('Desmaios').
sintoma('Desequilíbrio').
sintoma('Dificuldade de falar').
sintoma('Dificuldade para caminhar').
sintoma('Dificuldade para respirar').
sintoma('Dor de cabeça').
sintoma('Dor que fica mais intensa ao movimentar o corpo').
sintoma('Dor nas costas').
sintoma('Dor aguda no peito').
sintoma('Dor no peito').
sintoma('Desconforto torácico').
sintoma('Espirros constantes').
sintoma('Falta de apetite').
sintoma('Falta de ar').
sintoma('Cansaço (mesmo em repouso)').
sintoma('Formigamento de um lado do corpo, atingindo rosto, braço ou perna').
sintoma('Febre').
sintoma('Irritabilidade').
sintoma('Inchaço em diferentes áreas do corpo (extremidades e abdômem)').
sintoma('Irritação na garganta').
sintoma('Lábios roxos').
sintoma('Náuseas e vômitos').
sintoma('Nariz entupido').
sintoma('Queda da pálpebra').
sintoma('Respiração rápida e curta').
sintoma('Sensação de aperto no peito').
sintoma('Sensação de que o coração para de bater momentaneamente').
sintoma('Sensação de queimação na coluna').
sintoma('Sensação de latejamento').
sintoma('Sensação de sufoco').
sintoma('Sensação de peito carregado').
sintoma('Sensibilidade à luz').
sintoma('Sensibilidade aos cheiros').
sintoma('Sensibilidade aos sons').
sintoma('Tontura').
sintoma('Tosse').
sintoma('Vertigem').
sintoma('Visão borrada').
sintoma('Zumbido no ouvido').


/* ************************************************** */


% Funçoes
%

diagnostico_menu :-
    listar_sintomas(L), nl, nl, nl,
    write('Entre com uma lista de sintomas (apenas números) separados por vírgula:'), nl, nl, nl,
    read_string(user_input, "\n", "\r\t", _, Sintomas), nl, nl, nl,
    write('   *** ATENÇAO! ***

    O RESULTADO DESTE PROTOTIPO É APENAS INFORMATIVO.
    CONSULTE UM MÉDICO PARA UM DIAGNóSTICO CORRETO E PRECISO.'), nl, nl, nl,
    write('Pressione ENTER para continuar...'),
    read_string(user_input, "\n", "\r\t", _, _),
    split_string(Sintomas, ",", " ", S1),
    nomear_sintomas(L, S1, S2),
    findall(D, doenca(D, _), D1),
    diagnosticar(S2, D1, R1),
    remove_empty(R1, Doencas),
    tamanho_listas(Doencas, R3, 1),
    sort(0, @>, R3, Ordem),
    print_result(Doencas, Ordem).

diagnosticar(_, [], _).
diagnosticar(S2, [D1 | L], [R | L2]) :-
    doenca(D1, SD),
    intersecao(S2, SD, RI),
    subtracao(SD, S2, RS),
    R = [D1, RI, RS],
    diagnosticar(S2, L, L2).

print_result(_, []).
print_result(D, [[QtdSintomas, Index] | L]) :-
    nth1(Index, D, [N, RI, RS]),
    nl, nl, nl, write('DOENÇA: '), write(N), nl, nl,
    write('SINTOMAS EM COMUM ('), write(QtdSintomas), write('): '), nl, print_lista(RI), nl,
    write('OUTROS SINTOMAS: '), nl, print_lista(RS), nl, nl, nl,
    write('Pressione ENTER para continuar...'), nl, nl, nl,
    read_string(user_input, "\n", "\r\t", _, _),
    print_result(D, L).

listar_sintomas(S):-
    nl, nl, nl, write('    SINTOMAS'), nl, nl,
    findall(X, sintoma(X), L),
    sort(L, S),
    escrever_sintoma(S, 1).

escrever_sintoma([],_).
escrever_sintoma([S | L], N) :-
    write('    '), write(N), write(' - '),  write(S), nl,
    N2 is N + 1,
    escrever_sintoma(L, N2).

% Recebe uma lista de [N, LI, LO], em que:
%    N é o nome da doenca
%    LI é uma lista de intersecao de sintomas (usuario/doenca)
%    LO é uma lista de outros sintomas da doenca, que nao LI
% remove os elementos em que a LI esta vazia e devolve a lista R
remove_empty([], []).
remove_empty([[_, LI, _] | L], R) :-
    is_empty(LI),
    remove_empty(L, R).
remove_empty([[N, LI, LO] | L], [[N, LI, LO] | R]) :-
    \+ is_empty(LI),
    remove_empty(L, R).

is_empty([]).

% Recebe a lista de sintomas em ordem alfabética e "nomeia"
% os sintomas fornecidos pelo usuario
nomear_sintomas(_, [], _) :- !.
nomear_sintomas(L, [S | L1], [R | L2]) :-
    atom_number(S, N),
    nth1(N, L, R),
    nomear_sintomas(L, L1, L2).

print_lista([]).
print_lista([X | L]) :-
    write('    '), write(X), nl,
    print_lista(L).

% intersecao(L1, L2, R)
% recebe duas listas L1 e L2 e retorna a interseção entre elas em R
%
intersecao([], _, []).
intersecao([X | L1], L2, [X | L3]) :-
    member(X, L2),
    intersecao(L1, L2, L3), !.
intersecao([_ | L1], L2, R) :-
    intersecao(L1, L2,  R).

% subtracao(L1, L2, R)
% recebe duas listas L1 e L2 e retorna R = L1 - L2
%
subtracao([], _, []).
subtracao([X | L1], L2, [X | L3]) :-
    \+ member(X, L2),
    subtracao(L1, L2, L3), !.
subtracao([_ | L1], L2, L3) :-
    subtracao(L1, L2,  L3).

tamanho_listas([], [], _).
tamanho_listas([[_, S, _] | L1], [P | L2], N) :-
    length(S, Len),
    P = [Len, N],
    N1 is N + 1,
    tamanho_listas(L1, L2, N1).


:- begin_tests(modulo_diagnostico).

test(nomear_sintomas_1) :- nomear_sintomas(['S1', 'S2', 'S3', 'S4'],
                                           ["1", "4"],
                                           ['S1', 'S4']).
test(nomear_sintomas_2) :- nomear_sintomas(['S1', 'S2'],
                                           [],
                                           []).

test(inter_1) :- intersecao(['S1', 'S2', 'S3', 'S4'],
                            ['S1', 'S3', 'S5'],
                            ['S1', 'S3']).
test(inter_2) :- intersecao(['S1', 'S2', 'S3', 'S4'],
                            [],
                            []).

test(sub_1) :- subtracao(['S1', 'S2', 'S3', 'S4'],
                         ['S1', 'S3', 'S5'],
                         ['S2', 'S4']).
test(sub_2) :- subtracao(['S1', 'S2', 'S3', 'S4'],
                         [],
                         ['S1', 'S2', 'S3', 'S4']).

test(remove_empty_1) :- remove_empty([['D1', [], [1, 2, 3]], ['D2', ['Not Empty'], []]],
                                     [['D2', ['Not Empty'], []]]).
test(remove_empty_2) :- remove_empty([], []).

:- end_tests(modulo_diagnostico).










